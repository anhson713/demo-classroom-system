package org.trainingjava.coreexception.exception;

import org.springframework.http.HttpStatus;

public class NotFoundException extends BaseException {

    public NotFoundException() {
        setStatus(HttpStatus.NOT_FOUND.value());
        setCode("org.trainingjava.coreexception.exception.NotFoundException");
    }
}
