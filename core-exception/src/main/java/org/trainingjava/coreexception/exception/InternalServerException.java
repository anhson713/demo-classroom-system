package org.trainingjava.coreexception.exception;

import org.springframework.http.HttpStatus;

public class InternalServerException extends BaseException {

    public InternalServerException() {
        setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
        setCode("org.trainingjava.coreexception.exception.InternalServerException");
    }
}
