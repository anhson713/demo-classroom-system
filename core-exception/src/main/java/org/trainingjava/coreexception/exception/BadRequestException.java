package org.trainingjava.coreexception.exception;

import org.springframework.http.HttpStatus;

public class BadRequestException extends BaseException {

    public BadRequestException() {
        setStatus(HttpStatus.BAD_REQUEST.value());
        setCode("org.trainingjava.coreexception.exception.BadRequestException");
    }
}
