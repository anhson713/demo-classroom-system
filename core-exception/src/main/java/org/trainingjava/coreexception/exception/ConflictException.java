package org.trainingjava.coreexception.exception;

import org.springframework.http.HttpStatus;

public class ConflictException extends BaseException {

    public ConflictException() {
        setStatus(HttpStatus.CONFLICT.value());
        setCode("org.trainingjava.coreexception.exception.ConflictException");
    }
}
