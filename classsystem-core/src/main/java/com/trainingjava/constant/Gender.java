package com.trainingjava.constant;

public enum Gender {
    FEMALE, MALE
}
