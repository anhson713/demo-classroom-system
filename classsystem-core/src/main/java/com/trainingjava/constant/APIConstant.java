package com.trainingjava.constant;

import static com.trainingjava.constant.APIConstant.Resource.CLASSROOM;

public class APIConstant {

    public static final String API_PREFIX = "/api";
    public static final String API_VERSION = "/v1";

    public static class Resource {
        public static final String CLASSROOM = "/classrooms";

        public static final String STUDENT = "/students";
    }

    public static class BaseUrl {
        public static final String CLASSROOM_URL = API_PREFIX + API_VERSION + CLASSROOM;
    }
}
