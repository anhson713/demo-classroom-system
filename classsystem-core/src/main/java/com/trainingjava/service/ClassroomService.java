package com.trainingjava.service;

import com.trainingjava.dto.request.ClassroomRequest;
import com.trainingjava.dto.response.ClassroomDetailResponse;
import com.trainingjava.dto.response.ClassroomResponse;
import com.trainingjava.entity.Classroom;
import com.trainingjava.service.base.BaseService;

import java.util.List;

public interface ClassroomService extends BaseService<Classroom, Long> {

    ClassroomResponse create(ClassroomRequest request);

    void deleteById(Long id);

    ClassroomDetailResponse get(Long id);

    List<ClassroomResponse> list();

    ClassroomResponse update(long id, ClassroomRequest request);

    void validateExist(Long id);

    void validateStudentNumberWhenAdding(Long id);

    void validateStudentWhenUpdating(Long id, Integer requestStudentNumber);
}
