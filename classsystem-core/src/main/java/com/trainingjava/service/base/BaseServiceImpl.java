package com.trainingjava.service.base;

import com.trainingjava.entity.BaseEntity;
import lombok.extern.slf4j.Slf4j;
import com.trainingjava.repository.base.BaseRepository;

import java.util.List;

@Slf4j
public class BaseServiceImpl<E extends BaseEntity, ID> implements BaseService<E, ID> {

    private final BaseRepository<E, ID> repository;

    public BaseServiceImpl(BaseRepository<E, ID> repository) {
        this.repository = repository;
    }

    @Override
    public E create(E entity) {
        log.info("(create)object : {}", entity);
        return repository.save(entity);
    }

    @Override
    public void delete(ID id) {
        log.info("(delete)id : {}", id);
        repository.deleteById(id);
    }

    @Override
    public E find(ID id) {
        log.info("(find)id : {}", id);
        return repository.findById(id).orElse(null);
    }

    @Override
    public E update(E entity) {
        log.info("(update)object : {}", entity);
        return repository.save(entity);
    }

    @Override
    public boolean isExist(ID id) {
        log.info("(isExist)id : {}", id);
        return repository.existsById(id);
    }
}
