package com.trainingjava.service.base;

import com.trainingjava.entity.BaseEntity;

public interface BaseService<E extends BaseEntity, ID> {

    E create(E entity);

    void delete(ID id);

    E find(ID id);

    E update(E entity);

    boolean isExist(ID id);
}
