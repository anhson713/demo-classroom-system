package com.trainingjava.service.impl;

import com.trainingjava.dto.request.StudentRequest;
import com.trainingjava.dto.response.StudentResponse;
import com.trainingjava.entity.Student;
import com.trainingjava.mapper.StudentMapper;
import com.trainingjava.repository.StudentRepository;
import com.trainingjava.service.StudentService;
import com.trainingjava.service.base.BaseServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.transaction.annotation.Transactional;
import org.trainingjava.coreexception.exception.NotFoundException;

import java.util.List;

@Slf4j
public class StudentServiceImpl extends BaseServiceImpl<Student, Long> implements StudentService {

    private final StudentRepository repository;

    private final StudentMapper mapper;

    public StudentServiceImpl(StudentRepository repository, StudentMapper mapper) {
        super(repository);
        this.repository = repository;
        this.mapper = mapper;
    }

    @Override
    @Transactional
    public StudentResponse create(StudentRequest request) {
        log.info("(create)request : {}", request);
        Student student = mapper.createEntity(request);
        student = create(student);
        return mapper.toDTO(student);
    }

    @Override
    public Integer countByClassroomId(Long classroomId) {
        log.info("(countByClassroomId)classroomId : {}", classroomId);
        return repository.countAllByClassroomId(classroomId).orElse(0);
    }

    @Override
    public void deleteAllByClassroomId(Long classroomId) {
        log.info("(deleteAllByClassroomId)classroomId : {}", classroomId);
        repository.deleteAllByClassroomId(classroomId);
    }

    @Override
    @Transactional
    public void deleteById(Long id) {
        log.info("(deleteById)id : {}", id);
        if (!isExist(id)) {
            log.error("(deleteById)id : {} --> NOT FOUND EXCEPTION", id);
            throw new NotFoundException();
        }
        delete(id);
    }

    @Override
    @Transactional(readOnly = true)
    public StudentResponse get(Long classroomId, Long id) {
        log.info("(get)classroomId : {}, id : {}", classroomId, id);
        Student student = repository.findByClassroomIdAndId(classroomId, id).orElseThrow(
                () -> {
                    log.error("(get)classroomId : {}, id : {} --> NOT FOUND EXCEPTION", classroomId, id);
                    throw new NotFoundException();
                }
        );
        return mapper.toDTO(student);
    }

    @Override
    @Transactional(readOnly = true)
    public List<StudentResponse> list(Long classroomId) {
        log.info("(list)classroomId : {}", classroomId);
        return mapper.toDTOList(repository.getStudentsByClassroomId(classroomId));
    }

    @Override
    public StudentResponse update(Long id, StudentRequest request) {
        log.info("(update)id : {}, request : {}", id, request);
        Student student = find(id);
        if (student == null) {
            log.error("(update)id : {} --> NOT FOUND EXCEPTION", id);
            throw new NotFoundException();
        }
        mapper.updateEntity(student, request);
        student = update(student);
        return mapper.toDTO(student);
    }
}
