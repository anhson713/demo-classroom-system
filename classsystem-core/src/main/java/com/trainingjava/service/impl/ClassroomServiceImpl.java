package com.trainingjava.service.impl;

import com.trainingjava.dto.request.ClassroomRequest;
import com.trainingjava.dto.response.ClassroomDetailResponse;
import com.trainingjava.dto.response.ClassroomResponse;
import com.trainingjava.entity.Classroom;
import com.trainingjava.mapper.ClassroomMapper;
import com.trainingjava.repository.ClassroomRepository;
import com.trainingjava.repository.projection.ClassroomProjection;
import com.trainingjava.service.ClassroomService;
import com.trainingjava.service.StudentService;
import com.trainingjava.service.base.BaseServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.transaction.annotation.Transactional;
import org.trainingjava.coreexception.exception.BadRequestException;
import org.trainingjava.coreexception.exception.NotFoundException;

import java.util.List;
import java.util.Objects;

@Slf4j
public class ClassroomServiceImpl extends BaseServiceImpl<Classroom, Long> implements ClassroomService {

    private final ClassroomRepository repository;

    private final StudentService studentService;

    private final ClassroomMapper mapper;

    public ClassroomServiceImpl(ClassroomRepository repository, StudentService studentService, ClassroomMapper mapper) {
        super(repository);
        this.repository = repository;
        this.studentService = studentService;
        this.mapper = mapper;
    }

    @Override
    @Transactional
    public ClassroomResponse create(ClassroomRequest request) {
        log.info("(create)request : {}", request);
        validateClassroomName(request.getName());
        Classroom classroom = mapper.createEntity(request);
        return mapper.toDTO(create(classroom));
    }

    private void validateClassroomName(String name) {
        log.info("validateClassroomName : {}", name);
        if (repository.existsByName(name)) {
            log.error("(validateClassroomName)name : {} --> BAD REQUEST", name);
            throw new BadRequestException();
        }
    }

    @Override
    @Transactional
    public void deleteById(Long id) {
        log.info("(deleteById)id : {}", id);
        if (!isExist(id)) {
            log.error("(deleteById)id : {} --> NOT FOUND", id);
            throw new NotFoundException();
        }
        studentService.deleteAllByClassroomId(id);
        delete(id);
    }

    @Override
    @Transactional(readOnly = true)
    public ClassroomDetailResponse get(Long id) {
        log.info("(get)id : {}", id);
        ClassroomProjection projection = repository.findDetailById(id);
        if (Objects.isNull(projection)) {
            log.error("(get)id : {} --> NOT FOUND EXCEPTION", id);
            throw new NotFoundException();
        }
        return ClassroomDetailResponse.from(projection);
    }

    @Override
    @Transactional(readOnly = true)
    public List<ClassroomResponse> list() {
        log.info("(list)");
        return mapper.toDTOList(repository.findAll());
    }

    @Override
    @Transactional
    public ClassroomResponse update(long id, ClassroomRequest request) {
        log.info("(update)id : {}, request : {}", id, request);
        Classroom classroom = find(id);
        if (classroom == null) {
            throw new NotFoundException();
        }
        mapper.updateEntity(classroom, request);
        return mapper.toDTO(update(classroom));
    }

    @Override
    @Transactional
    public void validateExist(Long id) {
        log.info("(validateExist)id : {}", id);
        if (!isExist(id)) {
            log.error("(validateExist)id : {} --> NOT FOUND EXCEPTION", id);
            throw new NotFoundException();
        }
    }

    @Override
    @Transactional(readOnly = true)
    public void validateStudentNumberWhenAdding(Long id) {
        log.info("(validateStudentNumberWhenAdding)id : {}", id);
        Integer studentNumber = findStudentNumber(id);
        if (studentNumber.equals(studentService.countByClassroomId(id))) {
            throw new BadRequestException();
        }
    }

    @Override
    public void validateStudentWhenUpdating(Long id, Integer requestStudentNumber) {
        log.info("(validateStudentNumberWhenUpdating)id : {}", id);
        Integer studentNumber = findStudentNumber(id);
        if (studentNumber > requestStudentNumber) {
            log.error("(validateStudentNumberWhenUpdating)id : {}, requestStudentNumber : {}", id, requestStudentNumber);
            throw new BadRequestException();
        }
    }

    private Integer findStudentNumber(Long id) {
        log.info("(findStudentNumber)id : {}", id);
        return repository.findStudentNumber(id).orElseThrow(
                () -> {
                    log.error("(validateStudentNumber)id : {} --> NOT FOUND EXCEPTION", id);
                    throw new NotFoundException();
                }
        );
    }
}
