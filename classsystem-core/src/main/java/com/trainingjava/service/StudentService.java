package com.trainingjava.service;

import com.trainingjava.dto.request.StudentRequest;
import com.trainingjava.dto.response.StudentResponse;
import com.trainingjava.entity.Student;
import com.trainingjava.service.base.BaseService;

import java.util.List;

public interface StudentService extends BaseService<Student, Long> {

    StudentResponse create(StudentRequest request);

    Integer countByClassroomId(Long classroomId);

    void deleteAllByClassroomId(Long classroomId);

    void deleteById(Long id);

    StudentResponse get(Long classroomId, Long id);

    List<StudentResponse> list(Long classroomId);

    StudentResponse update(Long id, StudentRequest request);
}
