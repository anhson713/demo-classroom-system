package com.trainingjava.utils;

import java.time.LocalDate;
import java.util.Objects;

public class ValidationUtils {

    private ValidationUtils() {}

    public static boolean validateDateInteger(Integer integerDate) {

        if (Objects.isNull(integerDate)) {
            return true;
        }
        try {
            LocalDate localDate = DateUtils.convertIntegerToLocalDate(integerDate);
        } catch (Exception e) {
            return false;
        }
        return true;
    }
}
