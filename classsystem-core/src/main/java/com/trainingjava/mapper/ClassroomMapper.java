package com.trainingjava.mapper;

import com.trainingjava.dto.request.ClassroomRequest;
import com.trainingjava.dto.response.ClassroomResponse;
import com.trainingjava.entity.Classroom;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;

import java.util.List;

@Mapper(componentModel = "spring")
public interface ClassroomMapper {

    Classroom createEntity(ClassroomRequest classroomRequest);

    void updateEntity(@MappingTarget Classroom classroom, ClassroomRequest classroomRequest);

    ClassroomResponse toDTO(Classroom classroom);

    List<ClassroomResponse> toDTOList(List<Classroom> classrooms);
}
