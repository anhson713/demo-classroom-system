package com.trainingjava.mapper;

import com.trainingjava.dto.request.StudentRequest;
import com.trainingjava.dto.response.StudentResponse;
import com.trainingjava.entity.Student;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;

import java.util.List;

@Mapper(componentModel = "spring")
public interface StudentMapper {

    Student createEntity(StudentRequest createStudentRequest);

    void updateEntity(@MappingTarget Student student, StudentRequest studentRequest);

    StudentResponse toDTO(Student student);

    List<StudentResponse> toDTOList(List<Student> students);
}
