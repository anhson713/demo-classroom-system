package com.trainingjava.repository;

import com.trainingjava.repository.projection.ClassroomProjection;
import com.trainingjava.repository.transformer.ClassroomProjectionTransformer;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;

@Repository
public class CustomClassroomRepositoryImpl implements CustomClassroomRepository {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public ClassroomProjection findDetailById(Long classroomId) {
        return (ClassroomProjection) entityManager.createQuery(
                "select c.id as c_id," +
                        "c.name as c_name," +
                        "c.studentNumber as c_stu_num," +
                        "s.id as s_id," +
                        "s.name as s_name," +
                        "s.gender as s_gender," +
                        "s.dob as s_dob," +
                        "s.email as s_email," +
                        "s.phone as s_phone," +
                        "s.classroomId as s_classroom_id" +
                        " from Classroom c " +
                        "inner join Student s on c.id = s.classroomId")
                .unwrap(Query.class)
                .setTupleTransformer(new ClassroomProjectionTransformer())
                .getSingleResultOrNull();
    }
}
