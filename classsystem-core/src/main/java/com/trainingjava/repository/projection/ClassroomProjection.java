package com.trainingjava.repository.projection;

import com.trainingjava.constant.Gender;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
@Data
public class ClassroomProjection {

    public static class Alias {
        public static final String ID = "c_id";
        public static final String NAME = "c_name";
        public static final String STUDENT_NUMBERS = "c_stu_num";
    }

    private long id;

    private String name;

    private int studentNumber;

    public ClassroomProjection(Object[] tuples, Map<String, Integer> aliasesMap) {
        this.id = (long) tuples[aliasesMap.get(Alias.ID)];
        this.name = (String) tuples[aliasesMap.get(Alias.NAME)];
        this.studentNumber = (int) tuples[aliasesMap.get(Alias.STUDENT_NUMBERS)];
    }
    private List<StudentProjection> students = new ArrayList<>();
    @Data
    public static class StudentProjection {
        private long id;
        private String name;
        private Gender gender;
        private int dob;
        private String email;
        private String phone;

        private long classroomId;

        public static class Alias {
            public static final String ID = "s_id";
            public static final String NAME = "s_name";
            public static final String Gender = "s_gender";
            public static final String DOB = "s_dob";
            public static final String EMAIL = "s_email";
            public static final String PHONE = "s_phone";
            public static final String CLASSROOM_ID = "s_classroom_id";
        }

        public StudentProjection(Object[] tuples, Map<String, Integer> aliasesMap) {
            this.id = (long) tuples[aliasesMap.get(Alias.ID)];
            this.name = (String) tuples[aliasesMap.get(Alias.NAME)];
            this.gender = (Gender) tuples[aliasesMap.get(Alias.Gender)];
            this.dob = (int) tuples[aliasesMap.get(Alias.DOB)];
            this.email = (String) tuples[aliasesMap.get(Alias.EMAIL)];
            this.phone = (String) tuples[aliasesMap.get(Alias.PHONE)];
            this.classroomId = (long) tuples[aliasesMap.get(Alias.CLASSROOM_ID)];
        }
    }
}
