package com.trainingjava.repository.transformer;

import com.trainingjava.repository.projection.ClassroomProjection;
import org.hibernate.query.TupleTransformer;

import java.util.LinkedHashMap;
import java.util.Map;

public class ClassroomProjectionTransformer implements TupleTransformer<ClassroomProjection> {

    private final Map<Long, ClassroomProjection> classroomProjectionMap = new LinkedHashMap<>();

    @Override
    public ClassroomProjection transformTuple(Object[] tuples, String[] aliases) {
        Map<String, Integer> aliasToIndexMap = aliasToIndexMap(aliases);

        long classroomId = (long) tuples[aliasToIndexMap.get(ClassroomProjection.Alias.ID)];

        ClassroomProjection classroomProjection = classroomProjectionMap.computeIfAbsent(
                classroomId,
                id -> new ClassroomProjection(tuples, aliasToIndexMap)
        );
        classroomProjection.getStudents().add(
                new ClassroomProjection.StudentProjection(tuples, aliasToIndexMap)
        );

        return classroomProjection;
    }

    private Map<String, Integer> aliasToIndexMap(String[] aliases) {
        Map<String, Integer> aliasToIndexMap = new LinkedHashMap<>();

        for (int i = 0; i < aliases.length; i++) {
            aliasToIndexMap.put(aliases[i], i);
        }

        return aliasToIndexMap;
    }
}
