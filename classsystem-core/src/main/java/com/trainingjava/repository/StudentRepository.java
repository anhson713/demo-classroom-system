package com.trainingjava.repository;

import com.trainingjava.entity.Student;
import com.trainingjava.repository.base.BaseRepository;

import java.util.List;
import java.util.Optional;

public interface StudentRepository extends BaseRepository<Student, Long> {

    Optional<Student> findByClassroomIdAndId(Long classroomId, Long id);
    List<Student> getStudentsByClassroomId(Long classroomId);
    void deleteAllByClassroomId(Long classroomId);
    Optional<Integer> countAllByClassroomId(Long classroomId);
}
