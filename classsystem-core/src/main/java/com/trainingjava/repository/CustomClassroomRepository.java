package com.trainingjava.repository;

import com.trainingjava.repository.projection.ClassroomProjection;

public interface CustomClassroomRepository {

    ClassroomProjection findDetailById(Long classroomId);
}
