package com.trainingjava.repository;

import com.trainingjava.entity.Classroom;
import com.trainingjava.repository.base.BaseRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Optional;

public interface ClassroomRepository extends BaseRepository<Classroom, Long>, CustomClassroomRepository  {

    Boolean existsByName(String name);

    @Query("select c.studentNumber from Classroom c where c.id = :id")
    Optional<Integer> findStudentNumber(Long id);
}
