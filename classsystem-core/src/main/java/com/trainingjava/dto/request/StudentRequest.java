package com.trainingjava.dto.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.trainingjava.constant.Gender;
import com.trainingjava.validation.ValidateDateInteger;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.trainingjava.coreexception.exception.BadRequestException;

@Data
@Slf4j
public class StudentRequest {

    @JsonProperty("name")
    @NotBlank
    private String name;

    @JsonProperty("gender")
    @NotNull
    private Gender gender;

    @JsonProperty("dob")
    @ValidateDateInteger
    private Integer dob;

    @JsonProperty("email")
    private String email;

    @JsonProperty("phone")
    private String phone;

    @JsonProperty("classroom_id")
    @NotNull
    private Long classroomId;

    public void validateClassroomIdPathVariable(Long pathClassroomId) {
        if (!this.classroomId.equals(pathClassroomId)) {
            log.error("(validateClassroomIdPathVariable)classroomId : {}, pathId : {} --> BAD REQUEST EXCEPTION", classroomId, pathClassroomId);
            throw new BadRequestException();
        }
    }
}
