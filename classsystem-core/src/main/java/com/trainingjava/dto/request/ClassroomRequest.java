package com.trainingjava.dto.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.Data;

@Data
public class ClassroomRequest {

    @JsonProperty("name")
    @NotBlank
    private String name;

    @JsonProperty("student_number")
    @NotNull
    @Min(1)
    private Integer studentNumber;
}
