package com.trainingjava.dto.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.trainingjava.constant.Gender;
import lombok.Data;

@Data
public class StudentResponse {

    @JsonProperty("ịd")
    private long id;

    @JsonProperty("name")
    private String name;

    @JsonProperty("gender")
    private Gender gender;

    @JsonProperty("dob")
    private Integer dob;

    @JsonProperty("email")
    private String email;

    @JsonProperty("phone")
    private String phone;

    @JsonProperty("classroom_id")
    private Long classroomId;
}
