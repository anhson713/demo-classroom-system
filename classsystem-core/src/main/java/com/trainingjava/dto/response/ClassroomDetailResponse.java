package com.trainingjava.dto.response;

import com.trainingjava.repository.projection.ClassroomProjection;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@EqualsAndHashCode(callSuper = true)
@Data
@Slf4j
public class ClassroomDetailResponse extends ClassroomResponse {

    private List<StudentResponse> students = new ArrayList<>();

    public static ClassroomDetailResponse from(ClassroomProjection projection) {
        log.info("(from)projection : {}", projection);
        ClassroomDetailResponse detailResponse = new ClassroomDetailResponse();
        detailResponse.setId(projection.getId());
        detailResponse.setName(projection.getName());
        detailResponse.setStudentNumber(projection.getStudentNumber());
        projection.getStudents().forEach(student -> {
            StudentResponse response = new StudentResponse();
            response.setId(student.getId());
            response.setName(student.getName());
            response.setGender(student.getGender());
            response.setDob(student.getDob());
            response.setEmail(student.getEmail());
            response.setPhone(student.getPhone());
            response.setClassroomId(student.getClassroomId());
            detailResponse.addStudent(response);
        });
        return detailResponse;
    }

    private void addStudent(StudentResponse response) {
        this.students.add(response);
    }
}
