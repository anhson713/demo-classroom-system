package com.trainingjava.configuration;

import com.trainingjava.mapper.ClassroomMapper;
import com.trainingjava.mapper.StudentMapper;
import com.trainingjava.repository.ClassroomRepository;
import com.trainingjava.repository.StudentRepository;
import com.trainingjava.service.ClassroomService;
import com.trainingjava.service.StudentService;
import com.trainingjava.service.impl.ClassroomServiceImpl;
import com.trainingjava.service.impl.StudentServiceImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.trainingjava.coreexceptionapi.configuration.EnableCoreExceptionApi;

@Configuration
@EnableCoreExceptionApi
public class ClassSystemConfiguration {

    @Bean
    public ClassroomService classroomService(ClassroomRepository repository, StudentService studentService, ClassroomMapper mapper) {
        return new ClassroomServiceImpl(repository, studentService, mapper);
    }

    @Bean
    public StudentService studentService(StudentRepository repository, StudentMapper mapper) {
        return new StudentServiceImpl(repository, mapper);
    }
}
