package com.trainingjava.controller;

import com.trainingjava.dto.request.ClassroomRequest;
import com.trainingjava.dto.request.StudentRequest;
import com.trainingjava.dto.response.ClassroomDetailResponse;
import com.trainingjava.dto.response.ClassroomResponse;
import com.trainingjava.dto.response.StudentResponse;
import com.trainingjava.service.ClassroomService;
import com.trainingjava.service.StudentService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import static com.trainingjava.constant.APIConstant.BaseUrl.CLASSROOM_URL;
import static com.trainingjava.constant.APIConstant.Resource.STUDENT;

@RestController
@RequestMapping(CLASSROOM_URL)
@Slf4j
@RequiredArgsConstructor
public class ClassroomController {

    private final ClassroomService service;

    private final StudentService studentService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public ClassroomResponse create(@RequestBody @Validated ClassroomRequest request) {
        log.info("(create)request : {}", request);
        return service.create(request);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public void delete(@PathVariable long id) {
        log.info("(delete)id : {}", id);
        service.deleteById(id);
    }

    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public ClassroomDetailResponse get(@PathVariable("id")long id) {
        log.info("(get)id : {}", id);
        return service.get(id);
    }

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public List<ClassroomResponse> list() {
        log.info("(list)");
        return service.list();
    }

    @PutMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public ClassroomResponse update(@PathVariable long id,
                                    @RequestBody @Validated ClassroomRequest request) {
        log.info("(update)id : {}, request : {}", id, request);
        service.validateStudentWhenUpdating(id, request.getStudentNumber());
        return service.update(id, request);
    }

    @PostMapping("/{id}" + STUDENT)
    @ResponseStatus(HttpStatus.CREATED)
    public StudentResponse createStudent(@PathVariable("id") Long classroomId,
                                         @RequestBody @Validated StudentRequest request) {
        log.info("(createStudent)classroomId : {}, request : {}", classroomId, request);
        request.validateClassroomIdPathVariable(classroomId);
        service.validateExist(classroomId);
        service.validateStudentNumberWhenAdding(classroomId);
        return studentService.create(request);
    }

    @DeleteMapping("/{classroom_id}" + STUDENT + "/{student_id}")
    @ResponseStatus(HttpStatus.OK)
    public void deleteStudent(@PathVariable("classroom_id") Long classroomId,
                              @PathVariable("student_id") Long studentId) {
        log.info("(deleteStudent)classroomId : {}, studentId : {}", classroomId, studentId);
        service.validateExist(classroomId);
        studentService.deleteById(studentId);
    }

    @GetMapping("/{classroom_id}" + STUDENT + "/{student_id}")
    @ResponseStatus(HttpStatus.OK)
    public StudentResponse getStudent(@PathVariable("classroom_id") Long classroomId,
                               @PathVariable("student_id") Long studentId) {
        log.info("(get)classroomId : {}, studentId : {}", classroomId, studentId);
        service.validateExist(classroomId);
        return studentService.get(classroomId, studentId);
    }

    @GetMapping("/{id}" + STUDENT)
    @ResponseStatus(HttpStatus.OK)
    public List<StudentResponse> listStudent(@PathVariable("id") Long classroomId) {
        log.info("(list)id : {}", classroomId);
        service.validateExist(classroomId);
        return studentService.list(classroomId);
    }

    @PutMapping("/{classroom_id}" + STUDENT + "/{student_id}")
    @ResponseStatus(HttpStatus.OK)
    public StudentResponse updateStudent(@PathVariable("classroom_id") Long classroomId,
                                  @PathVariable("student_id") Long studentId,
                                  @RequestBody @Validated StudentRequest request) {
        log.info("(update)classroomId : {}, studentId : {}, request : {}", classroomId, studentId, request);
        request.validateClassroomIdPathVariable(classroomId);
        service.validateExist(classroomId);
        return studentService.update(studentId, request);
    }
}
