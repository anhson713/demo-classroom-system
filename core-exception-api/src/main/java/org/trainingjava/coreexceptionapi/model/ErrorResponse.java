package org.trainingjava.coreexceptionapi.model;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.time.Instant;

@Data
@AllArgsConstructor(staticName = "of")
public class ErrorResponse {

    private int status;

    private Instant timestamp;

    private Object data;
}
