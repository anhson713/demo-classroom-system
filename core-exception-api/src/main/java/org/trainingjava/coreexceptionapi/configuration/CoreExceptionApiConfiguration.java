package org.trainingjava.coreexceptionapi.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.trainingjava.coreexceptionapi.controller.CustomErrorHandler;

@Configuration
public class CoreExceptionApiConfiguration {
    @Bean
    public CustomErrorHandler customErrorHandler() {
        return new CustomErrorHandler();
    }
}
